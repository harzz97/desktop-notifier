package com.harzz.desktopnotifier.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.harzz.desktopnotifier.entity.Whatsapp
import com.harzz.desktopnotifier.repository.GenericRepository

class GenericViewModel (application: Application) : AndroidViewModel(application) {

    private var repository = GenericRepository(application)

    fun getMessages() = repository.getAllMessages()

    fun getMessagesFor(name: String) = repository.getMessagesFor(name)

    fun setMessage(msg: Whatsapp) = repository.setMessage(msg)

    fun getChats() = repository.getAllChats()

    fun getNotificationsFor(name: String) = repository.getNotificationsFor(name)
}