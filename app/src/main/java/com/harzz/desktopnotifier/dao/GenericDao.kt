package com.harzz.desktopnotifier.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.harzz.desktopnotifier.entity.Applications
import com.harzz.desktopnotifier.entity.OtherNotification
import com.harzz.desktopnotifier.entity.Whatsapp

@Dao
interface ApplicationsDao {
    @Query("select * from applications")
    fun getAllApplications(): List<Applications>

    @Insert
    fun insertApplication(applications: Applications)

    @Insert
    fun insertAppList(appList: List<Applications>)

    @Query("update applications set last_insert = :time where package_name = :appName")
    fun updateLastInsert(appName: String, time: Long)

}
@Dao
interface WhatsAppDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMessage(message: Whatsapp)

    @Query("select * from `com.whatsapp` order by time desc limit 20")
    fun getAllMessages() : LiveData<List<Whatsapp>>

    @Query("select * from `com.whatsapp` where title = :user order by time desc")
    fun getMessageFor(user: String) : LiveData<List<Whatsapp>>

    @Query("select distinct title from `com.whatsapp` order by time desc")
    fun getAllChats() : LiveData<List<String>>
}

@Dao
interface OtherNotificationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertNotification(notificationDao: OtherNotification)

    @Query("select * from otherNotifications")
    fun getAllNotifications(): LiveData<List<OtherNotification>>

    @Query(value = "select * from otherNotifications where packageName = :appName order by time desc")
    fun getNotificationsFor(appName: String): LiveData<List<OtherNotification>>
}