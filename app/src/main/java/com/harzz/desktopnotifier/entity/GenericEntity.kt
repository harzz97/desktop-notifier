package com.harzz.desktopnotifier.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "applications")
data class Applications(
    @PrimaryKey(autoGenerate = true)
    val uid: Int = 0,
    @ColumnInfo(name = "package_name") val appName: String,
    @ColumnInfo(name = "is_active") val isActive: Boolean,
    @ColumnInfo(name = "last_insert") val epoch: Long?
)

@Entity(tableName = "com.whatsapp")
data class Whatsapp(
    @PrimaryKey val msgHash : Long,
    @ColumnInfo(name = "title") val userName: String,
    @ColumnInfo(name = "message") val msg: String,
    @ColumnInfo(name = "time") val epoch: Long
)

@Entity(tableName = "otherNotifications")
data class OtherNotification(
    @PrimaryKey val msgHash: Long,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val desc: String,
    @ColumnInfo(name = "packageName") val pkgName: String,
    @ColumnInfo(name = "time") val epoch: Long
)
