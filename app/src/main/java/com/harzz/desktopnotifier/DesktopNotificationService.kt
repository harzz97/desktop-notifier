package com.harzz.desktopnotifier

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface DesktopNotificationService {
    @POST("notification")
    fun postNotification(@Body notification: Notification): Call<String>
}