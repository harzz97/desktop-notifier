package com.harzz.desktopnotifier

import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.wifi.WifiManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.room.Room
import com.harzz.desktopnotifier.database.GenericDatabase
import com.harzz.desktopnotifier.entity.OtherNotification
import com.harzz.desktopnotifier.entity.Whatsapp
import com.harzz.desktopnotifier.repository.GenericRepository
import retrofit2.Retrofit
import java.io.ByteArrayOutputStream
import java.sql.Date
import java.util.*


@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
class NotificationListener : NotificationListenerService() {
    override fun onCreate() {
        super.onCreate()
        Log.v("ACA","LOAD")
    }

    override fun onBind(intent: Intent): IBinder? {
        return super.onBind(intent)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onNotificationPosted(sbn: StatusBarNotification) {

        val appName = sbn.notification.extras.getCharSequence("android.title").toString()
        val desc = sbn.notification.extras.getCharSequence("android.text").toString()

        val packageName = sbn.packageName
        val hash = desc.plus(appName)
        val time = sbn.notification.`when`

        if (packageName == "com.whatsapp" && appName != "WhatsApp"){
            val message = Whatsapp(time.hashCode().toLong(),appName,desc,time)
            DatabaseTask(application, message).execute()
        } else {
            val oNotification = OtherNotification(time.hashCode().toLong(), appName,desc,packageName,time)
            NotificationDBTask(application,oNotification).execute()
        }

//        sbn.notification.hashCode()
//        sbn.notification.extras.keySet().forEach {
//            Log.e(it,sbn.notification.extras.get(it).toString())
//        }
//
//        val extras = sbn.notification.extras
//        if (extras.getCharSequenceArray("android.textLines") != null){
//            val text = Arrays.toString(extras.getCharSequenceArray("android.textLines"))
//            Log.v("TL",text)
//        }



        Log.v("Title", "$appName | $desc")

//        Log.i("HASH",appName+"\t"+desc.hashCode().toString())
        Log.v("WHEN",sbn.notification.`when`.toString())

//        val extras: Bundle = sbn.notification.extras
//
//        val iconId = extras.getInt(android.app.Notification.EXTRA_SMALL_ICON)
//
//        try {
//            val manager = packageManager
//            val resources: Resources = manager.getResourcesForApplication(packageName)
//            val icon: Drawable = resources.getDrawable(iconId)
//            val bmpDrawable = icon as BitmapDrawable
//            val bmp = sbn.notification.largeIcon
//            val byteArrayOutputStream = ByteArrayOutputStream()
//            bmp?.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
//            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
//            val encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
//            Log.v("ICON BASE ^$",encoded)
//
//        } catch (e: PackageManager.NameNotFoundException) {
//            e.printStackTrace()
//        }
//
//        if (extras.containsKey(android.app.Notification.EXTRA_PICTURE)) { // this bitmap contain the picture attachment
//            val bmp =
//                extras[android.app.Notification.EXTRA_PICTURE] as Bitmap?
//            val byteArrayOutputStream = ByteArrayOutputStream()
//            bmp?.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
//            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
//            val encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
//            Log.v("BASE ^$",encoded)
//
//        }

//        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
//        if (wifiManager.isWifiEnabled) {
//            val notification = Notification(appName,desc,packageName)
//            PostNotificationInBackground().execute(notification)
//        }
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {

    }

}
class Notification(var name: String, var desc: String, var packageName: String)

class PostNotificationInBackground : AsyncTask<Notification,Void,Void>(){

    override fun doInBackground(vararg params: Notification?): Void? {
        val retrofit: Retrofit? = NetworkClient().getRetrofitClient()

        val notificationService = retrofit?.create(DesktopNotificationService::class.java)

        val call = notificationService?.postNotification(params[0]!!)
        call?.execute()
        return null
    }
}

class DatabaseTask(var context: Application, var msg: Whatsapp) : AsyncTask<Void,Void,Void>(){

    lateinit var repository: GenericRepository
    override fun onPreExecute() {
        super.onPreExecute()
        repository = GenericRepository(context)
    }
    override fun doInBackground(vararg params: Void?): Void? {
        repository.setMessage(msg)
        return null
    }
}
class NotificationDBTask(var context: Application, var msg: OtherNotification) : AsyncTask<Void,Void,Void>(){

    lateinit var repository: GenericRepository
    override fun onPreExecute() {
        super.onPreExecute()
        repository = GenericRepository(context)
    }
    override fun doInBackground(vararg params: Void?): Void? {
        repository.setNotification(msg)
        return null
    }
}