package com.harzz.desktopnotifier.repository

import android.app.Application
import android.content.Context
import com.harzz.desktopnotifier.dao.OtherNotificationDao
import com.harzz.desktopnotifier.dao.WhatsAppDao
import com.harzz.desktopnotifier.database.GenericDatabase
import com.harzz.desktopnotifier.entity.OtherNotification
import com.harzz.desktopnotifier.entity.Whatsapp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class GenericRepository(application: Application) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var whatsAppDao : WhatsAppDao?
    private var otherNotificationDao : OtherNotificationDao?

    init {
        val db = GenericDatabase.getDatabase(application)
        whatsAppDao = db?.whatsappDao()
        otherNotificationDao = db?.otherNotificationsDao()
    }

    fun getAllMessages() = whatsAppDao?.getAllMessages()

    fun setMessage(msg: Whatsapp) {
        launch { setMessageInBackground(msg) }
    }

    fun getAllChats() = whatsAppDao?.getAllChats()

    fun getMessagesFor(name: String) = whatsAppDao?.getMessageFor(name)

    fun getNotificationsFor(name: String) = otherNotificationDao?.getNotificationsFor(name)

    fun setNotification(notification: OtherNotification){
        launch { setNotificationInBackground(notification) }
    }

    private suspend fun setMessageInBackground(msg: Whatsapp){
        withContext(Dispatchers.IO){
            whatsAppDao?.insertMessage(msg)
        }
    }
    private suspend fun setNotificationInBackground(msg: OtherNotification){
        withContext(Dispatchers.IO){
            otherNotificationDao?.insertNotification(msg)
        }
    }
}