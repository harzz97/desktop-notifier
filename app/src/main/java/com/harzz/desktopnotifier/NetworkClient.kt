package com.harzz.desktopnotifier

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkClient {

    val BASE_URL = "http://192.168.0.101:3000"

    var retrofit: Retrofit? = null
    var gson = GsonBuilder().setLenient().create()

    fun getRetrofitClient(): Retrofit? { //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit == null) { //Defining the Retrofit using Builder
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL) //This is the only mandatory call on Builder object.
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit
    }
}