package com.harzz.desktopnotifier.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.harzz.desktopnotifier.dao.ApplicationsDao
import com.harzz.desktopnotifier.dao.OtherNotificationDao
import com.harzz.desktopnotifier.dao.WhatsAppDao
import com.harzz.desktopnotifier.entity.Applications
import com.harzz.desktopnotifier.entity.OtherNotification
import com.harzz.desktopnotifier.entity.Whatsapp

@Database(entities = arrayOf(Applications::class,Whatsapp::class,OtherNotification::class), version = 1)
abstract class GenericDatabase : RoomDatabase() {
    abstract fun applicationDao() : ApplicationsDao
    abstract fun whatsappDao() : WhatsAppDao
    abstract fun otherNotificationsDao() : OtherNotificationDao

    companion object {

        @Volatile
        private var INSTANCE: GenericDatabase? = null

        fun getDatabase(context: Context): GenericDatabase? {
            if (INSTANCE == null) {
                synchronized(GenericDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            GenericDatabase::class.java, "Desktop-notifier"
                        )
                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}