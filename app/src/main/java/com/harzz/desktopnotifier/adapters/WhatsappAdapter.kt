package com.harzz.desktopnotifier.adapters

import android.content.Context
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.entity.Whatsapp
import kotlinx.android.synthetic.main.whatsapp_personal_row_layout.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.ExperimentalTime
import kotlin.time.days

class WhatsappAdapter(val context: Context,val message: List<Whatsapp>?) : RecyclerView.Adapter<MessageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder(LayoutInflater.from(context).inflate(R.layout.whatsapp_personal_row_layout,parent,false))
    }

    override fun getItemCount(): Int {
        return message?.size!!
    }

    @ExperimentalTime
    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.item_message.text = message?.get(position)!!.msg
        holder.item_time.text = getDateText(message[position].epoch)
    }

    @ExperimentalTime
    fun getDateText(epoch: Long): String {
        val msgDate = Date(epoch)
        val currentDate = Date()
        val customFormat = SimpleDateFormat("HH:mm",Locale.ENGLISH)
        return customFormat.format(msgDate).toString()
    }
}
class MessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var item_message = itemView.msg_item_desc
    var item_time = itemView.msg_item_time
}