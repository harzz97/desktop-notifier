package com.harzz.desktopnotifier.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.activities.WhatsappActivity
import kotlinx.android.synthetic.main.package_list_item.view.*
import java.util.ArrayList

class WhatsappChatAdapter(val context: Context, val chatList: ArrayList<String>) : RecyclerView.Adapter<ChatViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(LayoutInflater.from(context).inflate(R.layout.package_list_item,parent,false))
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        val chatName = holder.chatTitle
        chatName.text = chatList[position]
        holder.itemView.setOnClickListener {
            val intent = Intent(context,WhatsappActivity::class.java)
            intent.putExtra("chatName",chatName.text.toString())
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }
}
class ChatViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val chatTitle = view.packageName
}