package com.harzz.desktopnotifier.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.entity.OtherNotification
import kotlinx.android.synthetic.main.other_notification_row_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class OtherNotificationAdapter(val context: Context, val notificationList: List<OtherNotification>) :
        RecyclerView.Adapter<OtherNotificationViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OtherNotificationViewHolder {
        return OtherNotificationViewHolder(LayoutInflater.from(context).inflate(R.layout.other_notification_row_layout,parent,false))
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: OtherNotificationViewHolder, position: Int) {
        val item = holder.itemView
        holder.nTitle.text = notificationList[position].title
        holder.nDesc.text = notificationList[position].desc
        holder.nTime.text = getTime(notificationList[position].epoch)

    }

    private fun getTime(time: Long) : String {
        val msgDate = Date(time)
        val customFormat = SimpleDateFormat("DD MMM HH:mm", Locale.ENGLISH)
        return customFormat.format(msgDate).toString()
    }
}
class OtherNotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val nTitle = view.notification_title
    val nDesc = view.notification_desc
    val nTime = view.notification_time
}