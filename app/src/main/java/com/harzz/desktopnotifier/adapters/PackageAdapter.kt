package com.harzz.desktopnotifier.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.activities.AppInfo
import com.harzz.desktopnotifier.activities.OtherAppActivity
import com.harzz.desktopnotifier.activities.WhatsappActivity
import com.harzz.desktopnotifier.activities.WhatsappChatList
import kotlinx.android.synthetic.main.package_list_item.view.*
import java.util.*


class PackageAdapter(val context: Context,val appList: ArrayList<AppInfo>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(
                context
            ).inflate(
                R.layout.package_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return appList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.packageName.text = appList[position].label
//        holder.itemView.packageIcon.setImageDrawable(appList[position].icon)
        holder.itemView.setOnClickListener {
            if (it.packageName.text.toString().toLowerCase() == "whatsapp") {
                val intent = Intent(context, WhatsappChatList::class.java)
                context.startActivity(intent)
            }else {
                val intent = Intent(context, OtherAppActivity::class.java)
                intent.putExtra("appName",appList[position].packageName)
                context.startActivity(intent)
            }
        }
    }


}

class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val appName = view.packageName
}
