package com.harzz.desktopnotifier.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.adapters.OtherNotificationAdapter
import com.harzz.desktopnotifier.entity.OtherNotification
import com.harzz.desktopnotifier.entity.Whatsapp
import com.harzz.desktopnotifier.viewmodel.GenericViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.whatsapp_personal_chat.*

class OtherAppActivity: AppCompatActivity() {

    private var viewModel : GenericViewModel? = null
    lateinit var adapter: OtherNotificationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.whatsapp_personal_chat)

        val appName = intent.getStringExtra("appName")

        chat_name.text = appName

        viewModel = ViewModelProviders.of(this).get(GenericViewModel::class.java)

        viewModel?.getNotificationsFor(appName!!)?.observe(this, Observer<List<OtherNotification>> {this.renderNotifications(it)})

        chatsBackBtn.setOnClickListener {
            onBackPressed()
        }
    }

    private fun renderNotifications(nList: List<OtherNotification>){
        adapter = OtherNotificationAdapter(applicationContext,nList)
        chat_recycler.layoutManager = LinearLayoutManager(this)
        chat_recycler.adapter = adapter
    }

}