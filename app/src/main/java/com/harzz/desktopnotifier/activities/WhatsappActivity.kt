package com.harzz.desktopnotifier.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.adapters.WhatsappAdapter
import com.harzz.desktopnotifier.entity.Whatsapp
import com.harzz.desktopnotifier.viewmodel.GenericViewModel
import kotlinx.android.synthetic.main.whatsapp_personal_chat.*

class WhatsappActivity: AppCompatActivity() {

    private var messageViewModel: GenericViewModel? = null
    lateinit var messageAdapter: WhatsappAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.whatsapp_personal_chat)

        val chatName = intent.getStringExtra("chatName")
        chat_name.text = chatName
        messageViewModel = ViewModelProviders.of(this).get(GenericViewModel::class.java)

        messageViewModel?.getMessagesFor(chatName!!)?.observe(this, Observer<List<Whatsapp>> {this.renderMessages(it)})

        chatsBackBtn.setOnClickListener {
            onBackPressed()
        }
    }

    private fun renderMessages(messages: List<Whatsapp>) {
        Log.v("MSG CNT",messages.size.toString())
        messageAdapter = WhatsappAdapter(applicationContext, messages)
        chat_recycler.layoutManager = LinearLayoutManager(this)
        chat_recycler.adapter = messageAdapter
    }
}