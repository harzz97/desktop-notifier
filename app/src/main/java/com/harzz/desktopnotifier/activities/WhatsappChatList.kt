package com.harzz.desktopnotifier.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.adapters.WhatsappChatAdapter
import com.harzz.desktopnotifier.entity.Whatsapp
import com.harzz.desktopnotifier.repository.GenericRepository
import com.harzz.desktopnotifier.viewmodel.GenericViewModel
import kotlinx.android.synthetic.main.whatsapp_chat_list.*
import java.util.ArrayList

class WhatsappChatList: AppCompatActivity() {

    private var messageViewModel: GenericViewModel? = null
    lateinit var chatAdapter: WhatsappChatAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.whatsapp_chat_list)

        messageViewModel = ViewModelProviders.of(this).get(GenericViewModel::class.java)

        messageViewModel?.getChats()?.observe(this, Observer<List<String>> {this.renderChatHeads(it) })

        appBackBtn.setOnClickListener {
            onBackPressed()
        }
    }

    private fun renderChatHeads(chatList: List<String>) {
        chatAdapter = WhatsappChatAdapter(applicationContext, chatList as ArrayList<String>)
        chatHeadRecycler.layoutManager = LinearLayoutManager(this)
        chatHeadRecycler.adapter = chatAdapter
    }
}