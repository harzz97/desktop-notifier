package com.harzz.desktopnotifier.activities

import android.content.Context
import android.content.pm.ApplicationInfo
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.harzz.desktopnotifier.R
import com.harzz.desktopnotifier.adapters.PackageAdapter
import com.harzz.desktopnotifier.database.GenericDatabase
import com.harzz.desktopnotifier.entity.Applications
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val packagesList: ArrayList<AppInfo> = ArrayList()
    lateinit var database: GenericDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createPackageList()

        createApplicationsList()

        packageList.layoutManager = LinearLayoutManager(this)
        packageList.adapter = PackageAdapter(this,packagesList)

    }

    private fun createApplicationsList() {
        DBInsertTask(applicationContext).execute()
    }

    private fun createPackageList() {

        val pm = packageManager
        val apps = pm.getInstalledApplications(0)

        apps.forEach {

            val processName = it.packageName
            val labelName = pm.getApplicationLabel(it) as String
//            val icon = pm.getApplicationIcon(it)
//            val newApp = AppInfo(labelName)

            when {
                (it.flags and ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) !== 0 -> {
                    packagesList.add(AppInfo(labelName,processName))
                    //it's a system app, not interested
                }
                it.flags and ApplicationInfo.FLAG_SYSTEM !== 0 -> { //Discard this one
                    //in this case, it should be a user-installed app
                }
                else -> {
                    packagesList.add(AppInfo(labelName,processName))
                }
            }
            packagesList.sortBy {
                it.label
            }
            packagesList.reverse()
        }
    }
}
class AppInfo (var label: String, var packageName: String)

class DBInsertTask(var context: Context) : AsyncTask<Void,Void,Void>() {

    val appList: ArrayList<Applications> = ArrayList()
    lateinit var database: GenericDatabase
    override fun onPreExecute() {
        super.onPreExecute()
        database = GenericDatabase.getDatabase(context)!!
    }

    override fun doInBackground(vararg params: Void?) : Void? {

        if (database.applicationDao().getAllApplications().isEmpty()) {

            val pm = context.packageManager
            val apps = pm.getInstalledApplications(0)

            apps.forEach {
                val labelName = pm.getApplicationLabel(it) as String
                val app = Applications(0, labelName, true, null)
                appList.add(app)
            }

            Log.v("Task","Completed")
        }

        database.applicationDao().insertAppList(appList)
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Log.v("MA","POST EXECUTE")
    }

}